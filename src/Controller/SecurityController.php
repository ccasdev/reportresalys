<?php

namespace App\Controller;

use Exception;
use App\Entity\User;
use App\Form\ResetPassType;
use App\Form\RegistrationType;
use Doctrine\ORM\EntityManager;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Monolog\Handler\SwiftMailerHandler;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class SecurityController extends AbstractController
{
   /**
    * @Route("/inscription", name="security_registration")
    */
    public function registration(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder): Response
    {



        $user = new User;

        dump($request);

        $formRegistration = $this->createForm(RegistrationType::class, $user);
        $formRegistration->handleRequest($request);

        if( $formRegistration->isSubmitted() &&  $formRegistration->isValid())
        {
             $hash = $encoder->encodePassword($user, $user->getPassword());

             $user->setPassword($hash);



            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', 'Your account has been registered. LOGIN NOW');

            return $this->redirectToRoute('security_login');

        }
        return $this->render('security/registration.html.twig', [
            'formRegistration' => $formRegistration->createView()
        ]);
    }


    /**
     * @Route("/connexion", name="security_login")
     */

    public function login (AuthenticationUtils $authenticationUtils):Response
{

    

    $error = $authenticationUtils->getLastAuthenticationError();
    $lastUsername = $authenticationUtils->getLastUsername();
    
    
     $this->addFlash('error', 'Impossible de se connecter. Veuillez vérifier votre login et mot de passe');
    return $this->render('security/login.html.twig', [
                 'error' => $error,
                'last_username' => $lastUsername
            ]);
}

    // public function login(AuthenticationUtils $authenticationUtils):Response
    // {
    //     $error = $authenticationUtils->getLastAuthenticationError();
    //     $lastUsername = $authenticationUtils->getLastUsername();
    //     return $this->render('security/login.html.twig', [
    //         'error' => $error,
    //         'last_username' => $lastUsername
    //     ]);
    // }
                    
    


    /**
     * @Route("/deconnexion", name="security_logout")
     */

    public function logout()
    {
       
    }

    /**
     * @Route("/oubli-pass", name= "forgotten_password")
     */
    public function forgottenPass(Request $request, UserRepository $userRepo, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator)
    {

        $form = $this->createForm(ResetPassType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $donnes = $form->getData();

            $user = $userRepo->findOneByEmail($donnes['email']);

            if(!$user)
            {
                $this->addFlash('danger', 'Cette adresse existe pas');

                $this->redirectToRoute('security_login');
            }

            $token = $tokenGenerator->generateToken();

            try
            {
                $user->setResetToken($token);
                $entityManager = $this->getDoctrine()->getManager();    
                $entityManager->persist($user);
                $entityManager->flush();


            }catch(\Exception $e)
            {
                $this->addFlash('warning', 'Une erreur: '. $e->getMessage());
                return $this->redirectToRoute('security_login');
            }
            $url = $this->generateUrl('app_reset_password', ['token' => $token],
        UrlGeneratorInterface::ABSOLUTE_URL);
            $message =(new \Swift_Message('Mot de passe oublié'))
            ->setFrom('votre@adresse.fr')
            ->setTo($user->getEmail())
            ->setBody(
                "<p>Bonjour,</p><p> demande de réintialisation de mot de passe a été effectuée pour le " . $url . '</p>', 'text/html'
            );

            $mailer->send($message);

            $this->addFlash('message', 'un email de réintialisation de mot de passe a été envoyé');

            // return $this->redirectToRoute('security_login');
        }

        return $this->render('security/forgotten_password.html.twig', ['emailForm' => $form->createView()]);

    }

    /**
     * @Route("/reset-pass/{token}", name="app_reset_password")
     */

    public function resetPassword($token, Request $request, UserPasswordEncoderInterface $passwordEncoder) : Response
    {
        $user = $this->getDoctrine()->getRepository(user::class)->findOneBy(['reset_token' => $token]);
        //$user = $this->getDoctrine()->getRepository(user::class)->find($id);

        if(!$user)
        {
            $this->addFlash('danger', 'Token inconnu');
            return $this->redirectToRoute('security_login');
        }

        if ($request->isMethod('POST')) {

           $user->setResetToken($token);
          $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('_password')));
          

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('message', 'Mot de passe modifié success');
            return $this->redirectToRoute('security_login');


        }
        else
        {
            return $this->render('security/reset_password.html.twig', ['token' =>  $token]);
        }

    }
}
